#!/usr/bin/env python3

#################################
##         LIBRAIRIES          ##
#################################
import pytesseract
import re
import sys
from datetime import date
import base64
import io
import flask 
from flask import request
try:
    from PIL import Image
except ImportError:
    import Image
import csv
import json
import math
import os
import requests
from flask_cors import CORS

app = flask.Flask(__name__)
app.config["DEBUG"] = True
CORS(app)

#################################
## GET DLC WITH PICTURE        ##
## Param : imgBase64           ##
#################################
@app.route('/DLC', methods=['POST'])
def DLC():
    d=date.today()
    DLCFinale=str(int(d.day)+5)+"/"+str(d.month)+"/"+str(d.year)
    try :
        imgstring=request.json['imgBase64']
        if imgstring != "":
            imgstring = imgstring.split('base64,')[-1].strip()
            image_string = io.BytesIO(base64.b64decode(imgstring))
            data=pytesseract.image_to_string(Image.open(image_string)).split('\n')
            matchData=[]
            for row in data :
                matchOne = re.search(r'(\d+/\d+/\d+)',row.replace(' ','').replace('.','/'))
                #matchSecond = re.search(r'(.d+/.d+/.d+)',row.replace(' ',''))
                if matchOne != None:
                    matchData.append(matchOne.group(1))

            for DLC in matchData:
                if len(DLC.split('/')[2])>4:
                    DLCFinale=DLC[0:8]
                else:
                    DLCFinale=DLC
    except:
        print('Error in JSON format')
        #Mettre code retour ou voir pour l'erreur comment la gérer
    print(DLCFinale)
    return DLCFinale



#################################
## GET FRIDGE RECOMMENDATION   ##
## Param : iduser              ##
#################################
@app.route('/Recommandation', methods=['POST'])
def Recommandation():

    #Récupération param requete
    idUser=imgstring=request.json['iduser']

    #########################################
    #Algo recommandation recette pour frigo##
    #########################################

    #Data API Back-End
    API_URL="http://25.52.26.241:5001/"
    headers = {'Authorization': 'bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzYW1lcmUyIiwiaWF0IjoxNjQyNTgyODkwLCJleHAiOjE2NDI2NjkyOTB9.mYD8Nsz0dDNik_djP4kNLCPI9r1t3XylCAtY_eFL1ayysjbAXe8a89YhwfUqitL9FmQYMrm68T2SaLVfH3qBZw'}

    #InitData
    ingredientsUser=[]
    allergiesUser=[]
    avoidPreferenceIngredientUser=[]
    dictRecipeIngredient=[]

    allRecipe=[]
    allIngredients=[]

    #Tableau de recherche
    recipeToPropose=[]
    recipeOneElementMissing=[]
    recipeMoreElementMissing=[]

    #GET FILE OF ID INGREDIENT FOR RECIPE
    recipeIngPath = 'recipeIngredients.csv'
    with open(recipeIngPath,'r') as file_csv:
        fieldnames =("idrecipe","idingredients")
        readerIngredients = csv.DictReader(file_csv, fieldnames, delimiter=';')
        for recipe in readerIngredients:
            if recipe["idrecipe"]!='idrecipe':
                dictRecipeIngredient.append([recipe['idrecipe'],recipe['idingredients']])
    
    #GET FILE OF RECIPE
    recipePath = 'recipe.csv'
    with open(recipePath,'r') as file_csv:
        fieldnames =("id","name","imgurl","preparetime","cooktime","totaltime","directions","tags")
        readerRecipe = csv.DictReader(file_csv, fieldnames, delimiter=';')
        for recipe in readerRecipe:
            if recipe["id"]!='id':
                allRecipe.append([recipe['id'],recipe['name'],recipe['imgurl'],recipe['tags']])

    #GET USER
    urlUser=API_URL+"users/id/"+str(idUser)
    user= requests.get(url = urlUser, headers = headers)
    preferenceUser=user.json()['preferenceId']
    allergiesUser=user.json()['allergies']

    #GET USER FOODS
    urlFoodIngredients=API_URL+"fridges/user/"+str(idUser)+"?foods=true"
    foodIngredients= requests.get(url = urlFoodIngredients, headers = headers)
    for food in foodIngredients.json()[0]['foodList']:
            ingredientsUser.append([food['foodId'],food['dlc']])

    print(ingredientsUser)
    #GET FOOD
    urlFoods=API_URL+"foods/"
    foods= requests.get(url = urlFoods, headers = headers)
    allIngredients=foods.json()

    #GET AVOID INGREDIENT 
    # # #On récupère les preférences 
    # # with open(preferencesAvoidIngredientPath,'r') as file_csv:
    # #     fieldnames = ("idpreference","idingredient")
    # #     reader = csv.DictReader(file_csv, fieldnames, delimiter=';')
    # #     for data in reader:
    # #     if data["idpreference"]==preferenceUser:
    # #         avoidPreferenceIngredientUser.append(data['idingredient'])


    print(allergiesUser)
    #Algo de recommandation
    for recipeIngredient in dictRecipeIngredient:
        ingredients = recipeIngredient[1][1:-1].replace("'","").replace(" ","").split(',')
       # print("ingredients:")
        #print(ingredients)

        #On compare les elements d'une recette avec les élemnts que l'user à dans le frigo
        missingElement=0
        elementOk=0
        errorRecette=False
        for ing in ingredients:
            flag=0
            #On vérifier les allergies
            for allergie in allergiesUser:
                if str(allergie)==str(ing):
                    errorRecette=True
            # #On vérifier les ingrédients des préférences
            # for ingrPref in avoidPreferenceIngredientUser:
            #     if ingrPref==ing:
            #         errorRecette=True

            #On compare avec les élements du frigo
            for ingUser in ingredientsUser:
                if str(ing)==str(ingUser[0]):
                    elementOk+=1
                    flag=1
            if flag==0:
                missingElement+=1

        lenRecipe=len(recipeToPropose)
        if errorRecette is False:
            if lenRecipe<10:
                if missingElement== 0:
                    recipeToPropose.append(recipeIngredient[0])
                #Si un élement manquant alors recette oneElementMissing 
                elif missingElement ==1:
                    recipeOneElementMissing.append(recipeIngredient[0])
                #Si moins de 5 elements manquant et plus de 2 ok alors recette MoreElementMissing
                elif missingElement < 5 and elementOk >2:
                    recipeMoreElementMissing.append(recipeIngredient[0])

    #Conclusion 
                #     urlRecipe=API_URL+"recipes/"+str(recipeIngredient[0])
                # recipe= requests.get(url = urlRecipe, headers = headers)
                # recipe =recipe.json()
    recipeToReturn=[]
    lenRecipe=len(recipeToPropose)
    for recipeId in recipeToPropose :
        urlRecipe=API_URL+"recipes/"+str(recipeId)
        recipe= requests.get(url = urlRecipe, headers = headers)
        recipeToReturn.append(recipe.json())
    if lenRecipe<10:
        j=0
        while lenRecipe < 10 : 
            while j < len(recipeOneElementMissing) and lenRecipe < 10:
                urlRecipe=API_URL+"recipes/"+str(recipeOneElementMissing[j])
                recipe= requests.get(url = urlRecipe, headers = headers)
                recipeToReturn.append(recipe.json())
                j += 1 
                lenRecipe += 1
    print(recipeToPropose)
    return json.dumps(recipeToReturn)

app.run()
#app.run(host="0.0.0.0")
